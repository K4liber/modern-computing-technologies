import pandas as pd
import matplotlib.pyplot as plt
import numpy as np
from scipy.optimize import curve_fit
from scipy.misc import factorial
from scipy.stats import binom

# poisson function, parameter lamb is the fit parameter
def poisson(k, lamb):
    return (lamb**k/factorial(k)) * np.exp(-lamb)

df1 = pd.read_csv('degrees3000-4000.csv', names = ["N"])
entries1, bin_edges1, patches1 = plt.hist(df1['N'], bins=[0,1,2,3,4,5,6,7], density=True, label='Density')  # arguments are passed to np.histogram
bin_middles1 = 0.5*(bin_edges1[1:] + bin_edges1[:-1])
parameters1, cov_matrix1 = curve_fit(poisson, bin_middles1, entries1)
x_plot1 = np.linspace(0, 10, 100)
plt.plot(x_plot1, poisson(x_plot1, *parameters1), 'r-', lw=2, label='Poisson lambda = ' + str(parameters1[0]))
plt.legend(loc='upper right')
plt.title("Density of degrees, n = 3000, nnz = 4000")
plt.show()

plt.figure()

df1 = pd.read_csv('degrees100-500.csv', names = ["N"])
entries1, bin_edges1, patches1 = plt.hist(df1['N'], bins=[0,1,2,3,4,5,6,7,8,9,10,11,12,13], density=True, label='Density')  # arguments are passed to np.histogram
bin_middles1 = 0.5*(bin_edges1[1:] + bin_edges1[:-1])
parameters1, cov_matrix1 = curve_fit(poisson, bin_middles1, entries1)
x_plot1 = np.linspace(0, 13, 100)
plt.plot(x_plot1, poisson(x_plot1, *parameters1), 'r-', lw=2, label='Poisson = ' + str(parameters1[0]))
plt.legend(loc='upper right')
plt.title("Density of degrees, n = 100, nnz = 500")
plt.show()

plt.figure()

df1 = pd.read_csv('degrees1000-2000.csv', names = ["N"])
entries1, bin_edges1, patches1 = plt.hist(df1['N'], bins=[0,1,2,3,4,5,6,7,8,9,10,11,12], density=True, label='Density')  # arguments are passed to np.histogram
bin_middles1 = 0.5*(bin_edges1[1:] + bin_edges1[:-1])
parameters1, cov_matrix1 = curve_fit(poisson, bin_middles1, entries1)
x_plot1 = np.linspace(0, 12, 100)
plt.plot(x_plot1, poisson(x_plot1, *parameters1), 'r-', lw=2, label='Poisson = ' + str(parameters1[0]))
plt.legend(loc='upper right')
plt.title("Density of degrees, n = 1000, nnz = 2000")
plt.show()

plt.figure()

df1 = pd.read_csv('degrees8000-5000.csv', names = ["N"])
entries1, bin_edges1, patches1 = plt.hist(df1['N'], bins=[0,1,2,3,4,5,6,7], density=True, label='Density')  # arguments are passed to np.histogram
bin_middles1 = 0.5*(bin_edges1[1:] + bin_edges1[:-1])
parameters1, cov_matrix1 = curve_fit(poisson, bin_middles1, entries1)
x_plot1 = np.linspace(0, 7, 100)
plt.plot(x_plot1, poisson(x_plot1, *parameters1), 'r-', lw=2, label='Poisson = ' + str(parameters1[0]))
plt.legend(loc='upper right')
plt.title("Density of degrees, n = 8000, nnz = 5000")
plt.show()