#include <cstdio>
#include <iostream>
#include <cuda_runtime.h>
#include <cublas_v2.h>
#include <cstdlib>
#include <mpi.h>

using namespace std;

#define CUDA_CHECK(__err) \
do { \
    if (__err != cudaSuccess) { \
        fprintf(stderr, "Fatal error: %s (at %s:%d)\n", cudaGetErrorString(__err), __FILE__, __LINE__); \
        fprintf(stderr, "*** FAILED - ABORTING\n"); \
        exit(EXIT_FAILURE); \
    } \
} while (0)

#define CUBLASS_CHECK(__err) \
if (__err != CUBLAS_STATUS_SUCCESS) {\
        cublas_check(__err); \
        fprintf(stderr, "Fatal error at %s:%d\n", __FILE__, __LINE__); \
        fprintf(stderr, "*** FAILED - ABORTING\n"); \
        exit(EXIT_FAILURE); \
}

void cublas_check(cublasStatus_t error){
    switch (error)
    {
        case CUBLAS_STATUS_SUCCESS:
            break;

        case CUBLAS_STATUS_NOT_INITIALIZED:
            printf("CUBLAS_STATUS_NOT_INITIALIZED\n");
            break;

        case CUBLAS_STATUS_ALLOC_FAILED:
            printf("CUBLAS_STATUS_ALLOC_FAILED\n");
            break;

        case CUBLAS_STATUS_INVALID_VALUE:
            printf("CUBLAS_STATUS_INVALID_VALUE\n");
            break;

        case CUBLAS_STATUS_ARCH_MISMATCH:
            printf("CUBLAS_STATUS_ARCH_MISMATCH\n");
            break;

        case CUBLAS_STATUS_MAPPING_ERROR:
            printf("CUBLAS_STATUS_MAPPING_ERROR\n");
            break;

        case CUBLAS_STATUS_EXECUTION_FAILED:
            printf("CUBLAS_STATUS_EXECUTION_FAILED\n");
            break;

        case CUBLAS_STATUS_INTERNAL_ERROR:
            printf("CUBLAS_STATUS_INTERNAL_ERROR\n");
            break;
    }
}

void setVector(float* a, int count){
    for(int i=0; i<count; i++)
        a[i] = (float)i;   
}

void setZeroVector(float* a, int count) {
    for(int i=0; i<count; i++)
        a[i] = 0.0f;  
}

void setMatrix(float* a, int rows, int cols){
    for(int i=0; i<rows*cols; i++)
        a[i] = 1.0f;
}

int main(int argc, char** argv)
{   
    int rows = atoi(argv[1]), cols = atoi(argv[2]);
    // MPI 
    MPI_Comm amgx_mpi_comm = MPI_COMM_WORLD;
    MPI_Init(&argc, &argv);
    int rank = 0;
    int lrank = 0;
    int nranks = 0;
    int gpu_count = 0;
    MPI_Comm_size(amgx_mpi_comm, &nranks);
    MPI_Comm_rank(amgx_mpi_comm, &rank);
    CUDA_CHECK(cudaGetDeviceCount(&gpu_count));
    lrank = rank % gpu_count;
    int partRows = (int)rows/nranks;
    int startRow = rank*partRows;
    int stopRow = (rank+1)*partRows-1;
    CUDA_CHECK(cudaSetDevice(lrank));
    MPI_Barrier(amgx_mpi_comm);
    float elapsed=0;
    cudaEvent_t start, stop;

    if (rank == 0) {
        printf("Handling matrix %dx%d. Part size: %d\n", rows, cols, partRows);
        CUDA_CHECK(cudaEventCreate(&start));
        CUDA_CHECK(cudaEventCreate(&stop));
        CUDA_CHECK(cudaEventRecord(start, 0));
    }

    cublasHandle_t handle;
    CUBLASS_CHECK(cublasCreate(&handle));

    float *a, *y, *x, *partA, *partY;
    if (rank == 0) {
        y = (float*)malloc(sizeof(float) * rows);
        a = (float*)malloc(sizeof(float) * rows * cols);
        setZeroVector(y, rows);
        setMatrix(a, rows, cols);
    }
    partA = (float*)malloc(sizeof(float) * partRows * cols);
    partY = (float*)malloc(sizeof(float) * partRows);
    x = (float*)malloc(sizeof(float) * cols);
    MPI_Barrier(amgx_mpi_comm);
    MPI_Scatter(a, partRows * cols, MPI_FLOAT, partA,
        partRows * cols, MPI_FLOAT, 0, amgx_mpi_comm);

    MPI_Barrier(amgx_mpi_comm);
    printf("Process: %d, selecting device: %d, handling a[%d-%d] \n", rank, lrank, startRow, stopRow);
    setVector(x, cols);
    setZeroVector(partY, partRows);

    float *d_y = 0;
    float *d_x = 0;
    float *partD_a = 0;
    float *partD_y = 0;
    if (rank == 0) {
        CUDA_CHECK(cudaMalloc((void **)&d_y, rows * sizeof(float)));
        CUBLASS_CHECK(cublasSetVector(rows, sizeof(float), y, 1, d_y, 1));
    }
    
    CUDA_CHECK(cudaMalloc((void **)&d_x, cols * sizeof(float)));
    CUDA_CHECK(cudaMalloc((void **)&partD_y, partRows * sizeof(float)));
    CUDA_CHECK(cudaMalloc((void **)&partD_a, partRows * cols * sizeof(float)));
    CUDA_CHECK(cudaMemcpy(d_x, x, cols * sizeof(float), cudaMemcpyHostToDevice));
    CUDA_CHECK(cudaMemcpy(partD_y, partY, partRows * sizeof(float), cudaMemcpyHostToDevice));
    CUDA_CHECK(cudaMemcpy(partD_a, partA, partRows * cols * sizeof(float), cudaMemcpyHostToDevice));

    float alpha = 1.0f, beta = 1.0f;

    CUBLASS_CHECK(cublasSetPointerMode(handle, CUBLAS_POINTER_MODE_HOST));

    CUBLASS_CHECK(cublasSgemv(handle, 
        CUBLAS_OP_N, 
        partRows,
        cols, 
        &alpha, 
        partD_a, 
        partRows, 
        d_x, 
        1, 
        &beta, 
        partD_y, 
        1
    ));

    CUDA_CHECK(cudaMemcpy(partY, partD_y, partRows * sizeof(float), cudaMemcpyDeviceToHost));
    MPI_Barrier(amgx_mpi_comm);
    MPI_Gather(partY, partRows, MPI_FLOAT, y, partRows, MPI_FLOAT, 0, MPI_COMM_WORLD);
    
    if (rank == 0) {
        CUDA_CHECK(cudaEventRecord(stop, 0));
        CUDA_CHECK(cudaEventSynchronize(stop));
        CUDA_CHECK(cudaEventElapsedTime(&elapsed, start, stop) );
        CUDA_CHECK(cudaEventDestroy(start));
        CUDA_CHECK(cudaEventDestroy(stop));
        for (int i = 0; i < rows;i++) printf("y[%d] = %f\n", i, y[i]);
        printf("The elapsed time in gpu was %f ms\n", elapsed);
    }

    MPI_Finalize();
    return 0;
}