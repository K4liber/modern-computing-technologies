from __future__ import print_function, division

import numpy as np


mean = [0.,0.]
cov  = [[1.5,0.5],[0.5,3.0]]

data = np.random.multivariate_normal(mean,cov,2**24)
N = data.shape[0]
#x,y = np.random.multivariate_normal(mean,cov,2**20).T
x,y = data.T

cij = (x*y).mean()
m1  = x.mean()
m2  = y.mean()

print( cij          - m1*m2)
print((x**2).mean() - m1*m1)
print((y**2).mean() - m2*m2)
print()


new_cov = np.dot(data.T,data)
new_cov = new_cov/N - np.array([[m1*m1,m1*m2],[m2*m1,m2*m2]])
print(new_cov)

new_new_cov = np.zeros(new_cov.shape)
for i in range(2):
    data_chunk = data[i*(N//2):(i+1)*(N//2)]
    x,y = data_chunk.T
    m1  = x.mean()
    m2  = y.mean()
    new_cov = np.dot(data_chunk.T,data_chunk)
    new_cov = new_cov/(N//2) - np.array([[m1*m1,m1*m2],[m2*m1,m2*m2]])
    new_new_cov = new_new_cov + new_cov
print(new_new_cov/2)
print( np.abs(new_cov-new_new_cov/2) )


"""
import matplotlib.pyplot as plt
plt.xlim([-10.0, 10.0])
plt.ylim([-10.0, 10.0])
plt.scatter(x,y,s=0.1)
plt.show()
"""