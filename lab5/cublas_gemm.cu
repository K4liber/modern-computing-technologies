#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <cublas_v2.h> 

#define CUDA_CHECK(__err) \
do { \
    if (__err != cudaSuccess) { \
        fprintf(stderr, "Fatal error: %s (at %s:%d)\n", cudaGetErrorString(__err), __FILE__, __LINE__); \
        fprintf(stderr, "*** FAILED - ABORTING\n"); \
        exit(EXIT_FAILURE); \
    } \
} while (0)

void CUBLASS_CHECK(cublasStatus_t error)
{
    switch (error)
    {
        case CUBLAS_STATUS_SUCCESS:
            printf("CUBLAS_STATUS_SUCCESS");
            break;

        case CUBLAS_STATUS_NOT_INITIALIZED:
            printf("CUBLAS_STATUS_NOT_INITIALIZED");
            break;

        case CUBLAS_STATUS_ALLOC_FAILED:
            printf("CUBLAS_STATUS_ALLOC_FAILED");
            break;

        case CUBLAS_STATUS_INVALID_VALUE:
            printf("CUBLAS_STATUS_INVALID_VALUE");
            break;

        case CUBLAS_STATUS_ARCH_MISMATCH:
            printf("CUBLAS_STATUS_ARCH_MISMATCH");
            break;

        case CUBLAS_STATUS_MAPPING_ERROR:
            printf("CUBLAS_STATUS_MAPPING_ERROR");
            break;

        case CUBLAS_STATUS_EXECUTION_FAILED:
            printf("CUBLAS_STATUS_EXECUTION_FAILED");
            break;

        case CUBLAS_STATUS_INTERNAL_ERROR:
            printf("CUBLAS_STATUS_INTERNAL_ERROR");
            break;
    }
}

int main(int argc, char* argv[])
{
    int N = 100;
    float* h_x = (float*) malloc(sizeof(float) * N);
    float* h_b = (float*) malloc(sizeof(float) * N);
    float* h_A = (float*) malloc(sizeof(float) * N * N);

    
    for (int i = 0;i < N;i++) {
        h_x[i] = (float)i;
        h_b[i] = 0.0;
        for (int j = 0;j < N;j++) {
            h_A[i*N + j] = 1.0;
        }
    }

    float *d_b, *d_x, *d_A;
    
    size_t pitch;
    CUDA_CHECK(cudaMallocPitch(&d_A, &pitch, sizeof(float) * N, N));
    CUDA_CHECK(cudaMalloc(&d_x, sizeof(float) * N));
    CUDA_CHECK(cudaMalloc(&d_b, sizeof(float) * N));
    
    CUDA_CHECK(cudaMalloc((void **)&d_A, N * N * sizeof(float)));
    CUBLASS_CHECK(cublasSetVector(N * N, sizeof(float), h_A, 1, d_A, 1));

    CUDA_CHECK(cudaMemcpy(d_b, h_b, sizeof(float) * N, cudaMemcpyHostToDevice));
    CUDA_CHECK(cudaMemcpy(d_x, h_x, sizeof(float) * N, cudaMemcpyHostToDevice));

    CUDA_CHECK(cudaMemcpy2D(
        d_A, pitch,
        h_A, sizeof(float) * N,
        sizeof(float) * N, N, 
        cudaMemcpyHostToDevice
    ));
    
    cublasHandle_t handle;
    CUBLASS_CHECK(cublasCreate(&handle));

    float time_cuda_event;
    cudaEvent_t start, stop;    
    CUDA_CHECK(cudaEventCreate(&start));
    CUDA_CHECK(cudaEventCreate(&stop));
    CUDA_CHECK(cudaEventRecord(start, 0));
    
    // DO calculation here ...
    float alpha = 1.0f;
    float beta = 1.0f;
    CUBLASS_CHECK(cublasSgemv(handle, CUBLAS_OP_N, N, N, &alpha, d_A, N, d_x, 1, &beta, d_b, 1));

    
    CUDA_CHECK(cudaMemcpy(h_b, d_b, sizeof(float) * N, cudaMemcpyDeviceToHost));
    printf("b[19] = %f\n", h_b[19]);
    printf("A[19] = %f\n", h_A[19]);

    CUDA_CHECK(cudaEventRecord(stop, 0));
    CUDA_CHECK(cudaEventSynchronize(stop));
    CUDA_CHECK(cudaEventElapsedTime(&time_cuda_event, start, stop));              
    printf("Time :  %3.1f ms \n", time_cuda_event);  

    //cublasDestroy(handle);

    cudaFree(d_x);
    cudaFree(d_b);
    cudaFree(d_A);
    free(h_x);
    free(h_b);
    free(h_A);

    return EXIT_SUCCESS;
}