==5232== NVPROF is profiling process 5232, command: ./expected_val.exe
==5232== Profiling application: ./expected_val.exe
==5232== Profiling result:
"Time(%)","Time","Calls","Avg","Min","Max","Name"
%,ms,,ms,ms,ms,
26.045119,274.875329,100,2.748753,2.708674,3.015547,"void kernel_RC_mult<int=256, int=192, int=192, int=1>(double const *, double2 const *, double2*)"
20.375782,215.042200,100,2.150422,2.133231,2.291180,"void kernel_DZdreduce_threadfence<double, unsigned int=128, bool=0>(double const *, double2*, double*, unsigned int)"
19.856863,209.565621,100,2.095656,2.079792,2.124079,"void dot_kernel<double2, double2, double2, int=64, int=1, int=1>(cublasDotParams<double2, double2>)"
16.853296,177.866541,100,1.778665,1.757015,1.806967,"void thrust::system::cuda::detail::bulk_::detail::launch_by_value<unsigned int=128, thrust::system::cuda::detail::bulk_::detail::cuda_task<thrust::system::cuda::detail::bulk_::parallel_group<thrust::system::cuda::detail::bulk_::concurrent_group<thrust::system::cuda::detail::bulk_::agent<unsigned long=7>, unsigned long=128>, unsigned long=0>, thrust::system::cuda::detail::bulk_::detail::closure<thrust::system::cuda::detail::reduce_detail::reduce_partitions, thrust::tuple<thrust::system::cuda::detail::bulk_::detail::cursor<unsigned int=1>, thrust::transform_iterator<thrust::detail::zipped_binary_op<int, evaluate_functional>, thrust::zip_iterator<thrust::tuple<thrust::device_ptr<double>, thrust::device_ptr<double2>, thrust::null_type, thrust::null_type, thrust::null_type, thrust::null_type, thrust::null_type, thrust::null_type, thrust::null_type, thrust::null_type>>, int, thrust::use_default>, thrust::system::cuda::detail::aligned_decomposition<long>, thrust::detail::normal_iterator<thrust::pointer<int, thrust::system::cuda::detail::tag, thrust::use_default, thrust::use_default>>, int, thrust::plus<double>, thrust::null_type, thrust::null_type, thrust::null_type, thrust::null_type>>>>(unsigned long=7)"
14.780308,155.988615,100,1.559886,1.546205,1.824182,"void kernel_DZdreduce<unsigned int=512>(double*, double2*, double*, int)"
1.726463,18.220766,3,6.073588,0.000960,12.155529,"[CUDA memcpy HtoD]"
0.129855,1.370466,102,0.013435,0.003168,1.044712,"[CUDA memset]"
0.083150,0.877554,100,0.008775,0.008480,0.009728,"void thrust::system::cuda::detail::bulk_::detail::launch_by_value<unsigned int=128, thrust::system::cuda::detail::bulk_::detail::cuda_task<thrust::system::cuda::detail::bulk_::parallel_group<thrust::system::cuda::detail::bulk_::concurrent_group<thrust::system::cuda::detail::bulk_::agent<unsigned long=7>, unsigned long=128>, unsigned long=0>, thrust::system::cuda::detail::bulk_::detail::closure<thrust::system::cuda::detail::reduce_detail::reduce_partitions, thrust::tuple<thrust::system::cuda::detail::bulk_::detail::cursor<unsigned int=1>, thrust::detail::normal_iterator<thrust::pointer<int, thrust::system::cuda::detail::tag, thrust::use_default, thrust::use_default>>, thrust::detail::normal_iterator<thrust::pointer<int, thrust::system::cuda::detail::tag, thrust::use_default, thrust::use_default>>, thrust::detail::normal_iterator<thrust::pointer<int, thrust::system::cuda::detail::tag, thrust::use_default, thrust::use_default>>, thrust::plus<double>, thrust::null_type, thrust::null_type, thrust::null_type, thrust::null_type, thrust::null_type>>>>(unsigned long=7)"
0.054085,0.570803,400,0.001427,0.001344,0.001792,"[CUDA memcpy DtoH]"
0.040317,0.425498,100,0.004254,0.003904,0.004800,"void reduce_1Block_kernel<double2, double2, double2, int=64, int=6>(double2*, int, double2*)"
0.033704,0.355704,100,0.003557,0.003392,0.003840,"void __reduce_kernel__<unsigned int=512, double>(double*, double*, int)"
0.021057,0.222235,100,0.002222,0.002175,0.002368,"void __reduce_kernel__<unsigned int=64, double>(double*, double*, int)"

==5232== API calls:
No API activities were profiled.
