import numpy as np

sharedN100NNZ500 = [
    2.37334,
    2.40749,
    2.49734,
    2.41168,
    2.38394,
    2.43306,
    2.48579,
    2.41760,
    2.45517,
    2.42304
]

N100NNZ500 = [
    2.50992,
    2.50854,
    2.48054,
    2.46285,
    2.46483,
    2.49030,
    2.52061,
    2.45251,
    2.39245,
    2.46480
]

sharedN1000NNZ2000 = [
    5.28509,
    5.65526,
    5.49354,
    5.55696,
    5.22109,
    5.53702,
    5.16237,
    5.26157,
    5.62899,
    5.63485
]

N1000NNZ2000 = [
    5.38502,
    5.10182,
    5.52115,
    5.55264,
    5.58426,
    5.52979,
    5.57795,
    5.08602,
    5.78870,
    5.50934
]

sharedN3000NNZ4000 = [
    5.49472,
    5.69136,
    5.76330,
    5.90086,
    5.51578,
    5.70995,
    5.35917,
    5.63862,
    5.60237,
    5.39181
]

N3000NNZ4000 = [
    5.71069,
    5.78778,
    5.54678,
    5.25408,
    5.36262,
    5.61629,
    5.60314,
    5.81322,
    5.73168,
    5.69638
]

sharedN8000NNZ5000 = [
    6.00554,
    6.24544,
    6.01760,
    6.31590,
    6.26029,
    6.08246,
    6.30896,
    6.14778,
    6.24160,
    6.01056
]

N8000NNZ5000 = [
    5.99174,
    6.14864,
    6.33357,
    6.05213,
    6.25178,
    6.31613,
    5.88282,
    6.00077,
    6.20646,
    6.41165
]

print("sharedN100NNZ500: " + 
    str(np.mean(sharedN100NNZ500)) + 
    " (" + str(np.std(sharedN100NNZ500)) + 
    ") Min: " + str(np.min(sharedN100NNZ500)))

print("N100NNZ500: " + 
    str(np.mean(N100NNZ500)) + 
    " (" + str(np.std(N100NNZ500)) + 
    ") Min: " + str(np.min(N100NNZ500)))

print("sharedN1000NNZ2000: " + 
    str(np.mean(sharedN1000NNZ2000)) + 
    " (" + str(np.std(sharedN1000NNZ2000)) + 
    ") Min: " + str(np.min(sharedN1000NNZ2000)))

print("N1000NNZ2000: " + 
    str(np.mean(N1000NNZ2000)) + 
    " (" + str(np.std(N1000NNZ2000)) + 
    ") Min: " + str(np.min(N1000NNZ2000)))

print("sharedN3000NNZ4000: " + 
    str(np.mean(sharedN3000NNZ4000)) + 
    " (" + str(np.std(sharedN3000NNZ4000)) + 
    ") Min: " + str(np.min(sharedN3000NNZ4000)))

print("N3000NNZ4000: " + 
    str(np.mean(N3000NNZ4000)) + 
    " (" + str(np.std(N3000NNZ4000)) + 
    ") Min: " + str(np.min(N3000NNZ4000)))

print("sharedN8000NNZ5000: " + 
    str(np.mean(sharedN8000NNZ5000)) + 
    " (" + str(np.std(sharedN8000NNZ5000)) + 
    ") Min: " + str(np.min(sharedN8000NNZ5000)))

print("N8000NNZ5000: " + 
    str(np.mean(N8000NNZ5000)) + 
    " (" + str(np.std(N8000NNZ5000)) + 
    ") Min: " + str(np.min(N8000NNZ5000)))
