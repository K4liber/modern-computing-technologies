#include <stdio.h>
#include <cuda_runtime.h>
#include <cuda_runtime_api.h>

#define CUDA_CHECK(__err) \
    do { \
        if (__err != cudaSuccess) { \
            fprintf(stderr, "Fatal error: %s (at %s:%d)\n", cudaGetErrorString(__err), __FILE__, __LINE__); \
            fprintf(stderr, "*** FAILED - ABORTING\n"); \
            exit(EXIT_FAILURE); \
        } \
    } while (0)

__global__ 
void reduce0(double *g_idata, double *g_odata) {
  extern __shared__ double sdata[];
  // each thread loads one element from global to shared mem
  unsigned int tid = threadIdx.x;
  unsigned int i = blockIdx.x*blockDim.x + threadIdx.x;
  sdata[tid] = g_idata[i];
  __syncthreads();
  // do reduction in shared mem
  for (unsigned int s=1; s < blockDim.x; s *= 2) {
    if (tid % (2*s) == 0){
      sdata[tid] += sdata[tid + s];
    }
  __syncthreads();
  }
  // write result for this block to global mem
  if (tid == 0) g_odata[blockIdx.x] = sdata[0];
}

template <unsigned int blockSize>
__device__ void warpReduce(volatile double *sdata, unsigned int tid) {
  if (blockSize >= 64) sdata[tid] += sdata[tid + 32];
  if (blockSize >= 32) sdata[tid] += sdata[tid + 16];
  if (blockSize >= 16) sdata[tid] += sdata[tid + 8];
  if (blockSize >= 8) sdata[tid] += sdata[tid + 4];
  if (blockSize >= 4) sdata[tid] += sdata[tid + 2];
  if (blockSize >= 2) sdata[tid] += sdata[tid + 1];
}

template <unsigned int blockSize>
__global__ void reduce6(double *g_idata, double *g_odata, unsigned int n) {
  extern __shared__ double sdata[];
  unsigned int tid = threadIdx.x;
  unsigned int i = blockIdx.x*(blockSize*2) + tid;
  unsigned int gridSize = blockSize*2*gridDim.x;
  sdata[tid] = 0;

  while(i<n) { sdata[tid] += g_idata[i] + g_idata[i+blockSize]; i += gridSize; }
  __syncthreads();

  if(blockSize >= 512) { if (tid<256) {sdata[tid] += sdata[tid+256];} __syncthreads();}
  if(blockSize >= 256) { if (tid<128) {sdata[tid] += sdata[tid+128];} __syncthreads();}
  if(blockSize >= 128) { if (tid<64) {sdata[tid] += sdata[tid+64];} __syncthreads();}

  if (tid<32) warpReduce<blockSize>(sdata, tid);
  if (tid == 0) g_odata[blockIdx.x] = sdata[0];
}

/*
 * complie: nvcc -o main.x main.cu -O3 -gencode=arch=compute_35,code=sm_35 -gencode=arch=compute_37,code=sm_37 -gencode=arch=compute_60,code=sm_60
 */
int main(int *argc, char *argv[])
{
  float elapsed=0;
  cudaEvent_t start, stop;

  CUDA_CHECK(cudaEventCreate(&start));
  CUDA_CHECK(cudaEventCreate(&stop));

  int N = 1<<atoi(argv[1]);
  bool wrap = atoi(argv[2]);
  printf("Counting for N = %d\n", N);
  int maxN = 1024*32;
  double *h_x, *h_y, *d_x, *d_y;
  h_x = (double*)malloc(N*sizeof(double));
  h_y = (double*)malloc(maxN*sizeof(double));

  CUDA_CHECK( cudaMalloc(&d_x, N*sizeof(double)) ); 
  CUDA_CHECK( cudaMalloc(&d_y, maxN*sizeof(double)) );
  cudaDeviceSynchronize();

  for (int i = 0; i < N; i++) {
    h_x[i] = 1000000.0;
    if (i<maxN) h_y[i] = 0.0;
  }

  CUDA_CHECK(cudaEventRecord(start, 0));
  CUDA_CHECK( cudaMemcpy(d_x, h_x, N*sizeof(double), cudaMemcpyHostToDevice) );
  CUDA_CHECK( cudaMemcpy(d_y, h_y, maxN*sizeof(double), cudaMemcpyHostToDevice) );
  cudaDeviceSynchronize();
  if (wrap) {
    printf("(Wraping) Sum should be = %lf\n", 1000000.0*(float)N);
    reduce6<64><<<N/(1<<10), 1<<10, (1<<10)*sizeof(double)>>>(d_x, d_y, (unsigned int)N);
    cudaDeviceSynchronize();
    if (N == (1<<20)) {
      reduce6<64><<<1, 1<<10, (1<<10)*sizeof(double)>>>(d_y, d_y, (unsigned int)(1<<10));
    } else if (N == (1<<15)) {
      //reduce6<64><<<1, 1<<5, (1<<5)*sizeof(double)>>>(d_y, d_y, (unsigned int)(1<<5));
      reduce0<<<1, 1<<5, (1<<5)*sizeof(double)>>>(d_y, d_y);
    } else if (N == (1<<25)) {
      reduce6<64><<<32, 1<<10, (1<<10)*sizeof(double)>>>(d_y, d_y, (unsigned int)(32*1024));
      cudaDeviceSynchronize();
      //reduce6<32><<<1, 1<<5, (1<<5)*sizeof(double)>>>(d_y, d_y, (unsigned int)(32));
      reduce0<<<1, 1<<5, (1<<5)*sizeof(double)>>>(d_y, d_y);
    }
    cudaDeviceSynchronize();
  } else {
    printf("(No-Wraping) Sum should be = %lf\n", 1000000.0*(float)N);
    reduce0<<<N/(1<<10), 1<<10, (1<<10)*sizeof(double)>>>(d_x, d_y);
    cudaDeviceSynchronize();
    if (N == (1<<20)) {
      reduce0<<<1, 1<<10, (1<<10)*sizeof(double)>>>(d_y, d_y);
    } else if (N == (1<<15)) {
      reduce0<<<1, 1<<5, (1<<5)*sizeof(double)>>>(d_y, d_y);
    } else if (N == (1<<25)) {
      reduce0<<<N/(1<<20), 1<<10, (1<<10)*sizeof(double)>>>(d_y, d_y);
      cudaDeviceSynchronize();
      reduce0<<<1, 1<<5, (1<<5)*sizeof(double)>>>(d_y, d_y);
    }
    cudaDeviceSynchronize();
  }

  if (d_y) CUDA_CHECK( cudaMemcpy(h_y, d_y, 1*sizeof(double), cudaMemcpyDeviceToHost) );

  printf("Sum: %f\n", h_y[0]);

  CUDA_CHECK(cudaEventRecord(stop, 0));
  CUDA_CHECK(cudaEventSynchronize (stop) );
  CUDA_CHECK(cudaEventElapsedTime(&elapsed, start, stop) );
  CUDA_CHECK(cudaEventDestroy(start));
  CUDA_CHECK(cudaEventDestroy(stop));

  printf("The elapsed time in gpu was %.5f ms\n", elapsed);

  cudaFree(d_x);
  cudaFree(d_y);
  free(h_x);
  free(h_y);
}
