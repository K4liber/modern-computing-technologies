#ifdef _WIN32
#define WIN32_LEAN_AND_MEAN
#include <windows.h>
#else
#include <unistd.h>
#endif
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>

#include <type_traits> // for std::is_same

#include <omp.h>
#include <mpi.h>


// ===================================== serial version =======================================

template <typename T>
T mean_serial(T* vector, const int N)
{
    T mean = 0;
    for (int i=0; i<N; i++)
    {
        mean += vector[i];
    }
    return mean/N;
}


template <typename T>
T covariance_serial(T* vector1, T* vector2, const int N)
{
    T Cij = 0;
    T mean1 = mean_serial(vector1, N);
    T mean2 = mean_serial(vector2, N);
    
    for (int i=0; i<N; i++)
    {
        Cij += vector1[i]*vector2[i];
    }
    
    return Cij/N - mean1*mean2;
}


// ======================================= omp version ========================================


template <typename T, int nthreads>
T mean_omp(T* vector, const int N)
{
    T mean = 0;
    
    // TODO: What happens with #pragma omp simd
    #pragma omp parallel for reduction(+:mean) num_threads(nthreads)
    for (int i=0; i<N; i++)
    {
        mean += vector[i];
    }
    return mean/N;
}


template <typename T, int nthreads>
T covariance_omp(T* vector1, T* vector2, const int N)
{
    T Cij = 0;
    T mean1 = mean_omp<T,nthreads>(vector1, N);
    T mean2 = mean_omp<T,nthreads>(vector2, N);
    
    // count Cij here
    
    return Cij/N - mean1*mean2;
}



// ======================================= mpi version ========================================


template <typename T, int nthreads>
T mean_mpi(T* vector, const int N, MPI_Comm mpi_comm = MPI_COMM_WORLD)
{
    int ip, np;
    MPI_Comm_rank (mpi_comm, &ip);
    MPI_Comm_size (mpi_comm, &np);
    
    // TODO: evaluate mean value
    
    
    // TODO: here perform reduction
    T global_mean = 0;
    if      ( std::is_same<T,float>::value ) {
        fprintf(stderr, "Not implemented");
    }
    else if ( std::is_same<T,double>::value ) {
        fprintf(stderr, "LALAL");
    } else    
        fprintf(stderr,"ERROR: Type error in mean_mpi!\n");
    
    return global_mean/np;
}


template <typename T, int nthreads>
T covariance_mpi(T* vector1, T* vector2, const int N, MPI_Comm mpi_comm = MPI_COMM_WORLD)
{
    int ip, np;
    MPI_Comm_rank (mpi_comm, &ip);
    MPI_Comm_size (mpi_comm, &np);
    
    // TODO: evaluate covariance for each process
    
    
    
    // TODO: here perform reduction
    T covariance = 0;
    if      ( std::is_same<T,float>::value ) {
	fprintf(stderr, "Not implemented");
    } else if ( std::is_same<T,double>::value ) {
   	fprintf(stderr, "Not implemented");
    } else    
        fprintf(stderr,"ERROR: Type error in mean_mpi!\n");
    
    
    return covariance/np;
}



