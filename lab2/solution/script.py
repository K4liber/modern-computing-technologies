import pandas as pd
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
import numpy as np

df = pd.read_csv('data.csv',
        names = [
        "Processes", "Threads", "time"
        ])

meanDf = df.groupby(['Processes', 'Threads'], as_index=False).mean()

fig = plt.figure()
ax = fig.add_subplot(111, projection='3d')
ax.set_zlabel('time [s]')
ax.set_xlabel('#OpenMP threads')
ax.set_ylabel('#MPI processes')

x = y = np.arange(1, 7, 1)
X, Y = np.meshgrid(x, y)

print(np.array(meanDf['Threads']))

ax.plot_surface(X = X, 
                Y = Y, 
                Z = np.array(meanDf['time']).reshape(X.shape))
plt.show()