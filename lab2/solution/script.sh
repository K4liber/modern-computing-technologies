#!/bin/bash

for i in `seq 1 10`;
do
    for x in `seq 1 6`;
    do
	for y in `seq 1 6`;
	do
	    mpirun -np $x covariance_test.x -np $y
	done
    done
done