#include "FFTWDerivative.hpp"

#include <math.h>
#include <mpi.h>
#include <fftw3.h>
#include <fftw3-mpi.h>

int main(int argc, char **argv)
{
    
    MPI_Init(&argc, &argv);
    fftw_mpi_init();
    
    int ip, np;
    MPI_Comm_rank (MPI_COMM_WORLD, &ip);
    MPI_Comm_size (MPI_COMM_WORLD, &np);
    
    
    const int nx = 64;
    const int ny = 64;
    const int nz = 64;
    const int nxyz = nx*ny*nz;
    
    // NOTE: every process will allocate these arrays
    double* gaussian  = new double[nxyz]; // stores 3d gaussian function
    double* dgaussian = new double[nxyz]; // stores derivative over x of gaussian
    
    const double dx = 0.25;
    const double x0 = (-nx/2  )*dx;
    const double x1 = ( nx/2-1)*dx;
    const double y0 = (-ny/2  )*dx;
    const double y1 = ( ny/2-1)*dx;
    const double z0 = (-nz/2  )*dx;
    const double z1 = ( nz/2-1)*dx;
    const double Lx = x1-x0;
    const double Ly = y1-y0;
    const double Lz = z1-z0;
    
    for (int ix=-nx/2; ix < nx/2; ix++)
    for (int iy=-ny/2; ix < ny/2; iy++)
    for (int iz=-nz/2; ix < nz/2; iz++)
    {
        const int ixyz = (ix+nx/2)*ny*nz + (iy+ny/2)*nz + (iz+nz/2);
        const double x = ix*dx;
        const double y = iy*dx;
        const double z = iz*dx;
        
        gaussian[ixyz]  =    exp( -0.5*(x*x+y*y+z*z) );
    }
    
    
    // scatter data along all processes
    
    
    
    // create class for differentiation
    FFTWDerivative diff(nx,ny,nz,dx,dx,dx);
    
    // here compute derivative by FFTW with mpi
    diff.transform_forward(gaussian);
    diff.multiply_by_ikx();
    diff.transform_inverse(dgaussian); // now dgaussian should contain derivative over x
    
    
    
    if (ip == 0)
    {
        FILE* fp = fopen("test.txt","w");
        int iy = 0;
        int iz = 0;
        for (int ix=-nx/2; ix < nx/2; ix++)
        //for (int iy=-ny/2; ix < ny/2; iy++)
        //for (int iz=-nz/2; ix < nz/2; iz++)
        {
            const int ixyz = (ix+nx/2)*ny*nz + (iy+ny/2)*nz + (iz+nz/2);
            const double x = ix*dx;
            const double y = iy*dx;
            const double z = iz*dx;
            
            fprintf(fp,"%.10e \t %.10e \t %.10e \t ",x,y,z,x*exp(-0.5*(x*x+y*y+z*z)) ,dgaussian[ixyz]);
        }
        fclose(fp);
    }
    
    delete gaussian;
    delete dgaussian;
}
