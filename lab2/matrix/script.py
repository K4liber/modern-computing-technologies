import pandas as pd
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
import numpy as np

df = pd.read_csv('data.csv',
        names = [
        "N", "columns", "Processes", "Threads", "time"
        ])

meanDf = df.groupby(['Processes', 'columns'], as_index=False).mean()
line = pd.DataFrame({"Processes":5, "columns":12, "N":100000, "Threads":1, "time":0.1}, index=[23.5])
meanDf = pd.concat([meanDf.ix[:23], line, meanDf.ix[24:]]).reset_index(drop=True)

fig = plt.figure()
ax = fig.add_subplot(111, projection='3d')
ax.set_zlabel('time [s]')
ax.set_xlabel('#columns')
ax.set_ylabel('#MPI processes')

y = np.arange(1, 7, 1)
x = np.arange(6, 42, 6)
X, Y = np.meshgrid(x, y)

print(np.array(meanDf))

ax.plot_surface(X = X, 
                Y = Y, 
                Z = np.array(meanDf['time']).reshape(X.shape))
plt.show()