#pragma once

#include <mpi.h>
#include <complex.h>
#include <fftw3-mpi.h>


class FFTWDerivative
{
private:
     const long int nx, ny, nz; // TODO: move to template
     const double dx;
     const double dy;
     const double dz;
     double * kkx , * kky , * kkz ; /* reciprocal lattice */
     
     long int local_nx, local_x_start, total_local_size;
     
     fftw_plan fplan, iplan;
     double *data;
     __complex__ double *cdata;
    
public:
    FFTWDerivative(int _nx, int _ny, int _nz,
                   double _dx, double _dy, double _dz,
                   MPI_Comm mpi_comm = MPI_COMM_WORLD):
    nx(_nx),ny(_ny),nz(_nz), dx(_dx), dy(_dy), dz(_dz)
    {
        int ip, np;
        MPI_Comm_rank (mpi_comm, &ip);
        MPI_Comm_size (mpi_comm, &np);
        
        /* have to call MPI_Init(&argc, &argv) before */
        /* have to call fftw_mpi_init() before        */
        /* create the forward and backward plans      */
        //total_local_size = ...;
        data  =                       fftw_alloc_real(2 * total_local_size);
        cdata = (__complex__ double*) fftw_alloc_complex( total_local_size);
        
        //fplan = ...;
        
        //iplan = ...;
        
        
        /* ******************************  reciprocal lattice  ************************************* */
        
        kkx = new double[nx];
        kky = new double[ny];
        kkz = new double[nz];
        
        /* initialize the k-space lattice */
        int i,j;
        
        
    }
    
    ~FFTWDerivative()
    {
        delete kkx;
        delete kky;
        delete kkz;
        fftw_free(data);
        fftw_free(cdata);
        fftw_destroy_plan(fplan);
        fftw_destroy_plan(iplan);
    }
    
    
    void transform_forward(double* f)
    {
        /* initialize data */

        /* compute the forward transform */
        fftw_execute(fplan);
    }
    
    void multiply_by_ikx()
    {
        
    }
    
    void multiply_by_iky()
    {
        
    }
    
    void multiply_by_ikz()
    {
        
    }
    
    
    void transform_inverse(double* df)
    {
        /* compute the inverse transform, the result
        is transposed back to the original data layout */
        fftw_execute(iplan);
        
        /* copy back the data and normalize */
        const double nxyz = ((double) nx)*((double) ny)*((double) nz);
        
        
        // scatter data along all processes
        
    }
    
};

#undef I
