#include <iostream>
#include <cuda_runtime.h>
#include <cuda_runtime_api.h>
#include <thrust/host_vector.h>
#include <thrust/reduce.h>
#include <thrust/fill.h>

#define CUDA_CHECK(__err) \
    do { \
        if (__err != cudaSuccess) { \
            fprintf(stderr, "Fatal error: %s (at %s:%d)\n", cudaGetErrorString(__err), __FILE__, __LINE__); \
            fprintf(stderr, "*** FAILED - ABORTING\n"); \
            exit(EXIT_FAILURE); \
        } \
    } while (0)

// COMPILE:
// nvcc -o reduce.x reduce.cu -O3 -gencode=arch=compute_35,code=sm_35 -gencode=arch=compute_37,code=sm_37 -gencode=arch=compute_60,code=sm_60 -I/usr/local/cuda/include -L/usr/local/cuda/lib64 -lcudart -lm
int main(int *argc, char *argv[])
{
    float elapsed=0;
    cudaEvent_t start, stop;
  
    CUDA_CHECK(cudaEventCreate(&start));
    CUDA_CHECK(cudaEventCreate(&stop));

    int N = 1<<atoi(argv[1]);
    //double b[N];
    double *b = (double*)malloc(N*sizeof(double));
    printf("Counting for N = %d\n", N);
    printf("Sum should be = %lf\n", 1000000.0*(float)N);
    thrust::fill(b, b+N, 1000000.0);
    CUDA_CHECK(cudaEventRecord(start, 0));
    printf("Sum: %f\n", thrust::reduce(b, b+N));

    CUDA_CHECK(cudaEventRecord(stop, 0));
    CUDA_CHECK(cudaEventSynchronize (stop) );
    CUDA_CHECK(cudaEventElapsedTime(&elapsed, start, stop) );
    CUDA_CHECK(cudaEventDestroy(start));
    CUDA_CHECK(cudaEventDestroy(stop));

    printf("The elapsed time in gpu was %.5f ms\n", elapsed);

    return 0;
}