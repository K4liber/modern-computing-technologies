#!/bin/bash

for i in `seq 1 10`;
do
    for x in `seq 1 6`;
    do
	for y in `seq 10000 40000 210000`;
	do
	    mpirun -np $x covariance_test.x -np 1 $y 18
	done
    done
done