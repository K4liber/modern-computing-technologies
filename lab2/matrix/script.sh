#!/bin/bash

for i in `seq 1 10`;
do
    for x in `seq 1 6`;
    do
	for y in `seq 6 6 36`;
	do
	    mpirun -np $x covariance_test.x -np 1 100000 $y
	done
    done
done