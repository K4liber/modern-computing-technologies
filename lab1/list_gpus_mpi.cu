#include <stdio.h>
#include <stddef.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <string.h>
#include <assert.h>
#include <math.h>
#include <complex.h>

#ifdef _WIN32
#define WIN32_LEAN_AND_MEAN
#include <windows.h>
#else
#include <unistd.h>
#endif

#include <omp.h>
#include <mpi.h>
#include <fftw3-mpi.h>

inline void get_nprocessors(long* nprocs, long* nprocs_max)
{
    
#ifdef _WIN32
#ifndef _SC_NPROCESSORS_ONLN
    SYSTEM_INFO info;
    GetSystemInfo(&info);
#define sysconf(a) info.dwNumberOfProcessors
#define _SC_NPROCESSORS_ONLN
#endif
#endif
    
#ifdef _SC_NPROCESSORS_ONLN
    *nprocs = sysconf(_SC_NPROCESSORS_ONLN);
    if (*nprocs < 1)     {  fprintf(stderr, "Could not determine number of CPUs online:\n%s\n", strerror (errno)); exit (EXIT_FAILURE);  }
    
    *nprocs_max = sysconf(_SC_NPROCESSORS_CONF);
    if (*nprocs_max < 1) {  fprintf(stderr, "Could not determine number of CPUs configured:\n%s\n", strerror (errno)); exit (EXIT_FAILURE);  }
    
#else
  fprintf(stderr, "Could not determine number of CPUs");
  exit (EXIT_FAILURE);
#endif
}

void print_device(const int i)
{
    cudaDeviceProp prop;
            cudaGetDeviceProperties(&prop, i);
            printf("Device Number: %d\n", i);
            printf("  Device name: %s\n", prop.name);
            printf("  Memory Clock Rate (KHz):      %d\n", prop.memoryClockRate);
            printf("  Memory Bus Width (bits):      %d\n", prop.memoryBusWidth);
            printf("  Peak Memory Bandwidth (GB/s): %f\n", prop.memoryClockRate*2.0*(prop.memoryBusWidth/8)/1.0e6);
            printf("  Total global memory:          %d\n", prop.totalGlobalMem);
            printf("  No. of SMs:                   %d\n", prop.multiProcessorCount);
            printf("  Shared memory / SM:           %d\n", prop.sharedMemPerBlock);
            printf("  Registers/ SM:                %d\n", prop.regsPerBlock);
            printf("\n");
}


/*
 * compile: nvcc -o mpi_and_openmp.x mpi_and_openmp.cu -Xcompiler "-fPIC -fopenmp -O3 -mtune=native -march=native -pthread" -Xcompiler \"-Wl,-rpath,/usr/lib/openmpi/lib,--enable-new-dtags\" -I/usr/local/cuda/include -I/usr/lib/openmpi/include/openmpi/opal/mca/event/libevent2021/libevent -I/usr/lib/openmpi/include/openmpi/opal/mca/event/libevent2021/libevent/include -I/usr/lib/openmpi/include -I/usr/lib/openmpi/include/openmpi -L/usr/lib/openmpi/lib -L/usr/local/cuda/lib64 -lmpi -lcudart -lgomp -lm
 */
int main(int argc, char* argv[])
{
    int ierr; // error flag
    int ip, np; // basic MPI indicators
    
    /* start mpi */
    MPI_Init( &argc , &argv ) ; /* set up the parallel WORLD */
    MPI_Comm_size( MPI_COMM_WORLD , &np ) ; /* total number of processes */
    MPI_Comm_rank( MPI_COMM_WORLD , &ip ) ; /* id of process st 0 <= ip < np */
    
    
    /* get node name */
    int name_len;
    char processor_name[MPI_MAX_PROCESSOR_NAME];
    MPI_Get_processor_name(processor_name, &name_len);
    
    
    long nprocs = -1;
    long nprocs_max = -1;
    
    get_nprocessors(&nprocs,&nprocs_max);
    
    
    int n_devices;
    cudaGetDeviceCount(&n_devices);
    
    /*
    omp_set_dynamic(0);     // Explicitly disable dynamic teams
    omp_set_num_threads(4); // Use 4 threads for all consecutive parallel regions
    #pragma omp parallel
    {
    int tid = omp_get_thread_num();
    printf("MPI process id: %d\tOpenMP thread id: %d\n",ip,tid);
    
    // synchronize
    if (tid == 0) MPI_Barrier( MPI_COMM_WORLD );
    #pragma omp barrier
    
    if (tid == 0)
    {
        printf("processor_name: %s\n",processor_name);
        printf("device number:  %d\n",n_devices);
        for (int i = 0; i < n_devices; i++)  print_device(i);
        printf("=======================================================================================\n");
        printf("\n");
    }
    
    } // end parallel section
    */
    
    for (int j=0; j < np; j++)
    {
        if (ip == j)
        {
            printf("processor_name: %s\n",processor_name);
            printf("device number:  %d\n",n_devices);
            for (int i = 0; i < n_devices; i++)  print_device(i);
            printf("=======================================================================================\n");
            printf("\n");
            MPI_Barrier( MPI_COMM_WORLD );
        }
        
    }
    
    
    /* messy exit here */
    MPI_Barrier( MPI_COMM_WORLD ) ;
    MPI_Finalize() ;
    
    return EXIT_SUCCESS;
}









