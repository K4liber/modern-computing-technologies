import numpy as np

dataCUDA = [
    17.68720,
    17.63882,
    17.67229,
    17.40650,
    17.44781,
    18.16406,
    17.63603,
    17.45856,
    18.11866,
    17.34499
]

data0CUDA15 = [
    0.45635,
    0.45795,
    0.44160,
    0.60218,
    0.60122,
    0.57891,
    0.46902,
    0.43878,
    0.44294,
    0.44422
]

data7CUDA15 = [
    0.44454,
    0.49235,
    0.46003,
    0.48163,
    0.52157,
    0.48794,
    0.45491,
    0.48678,
    0.44323,
    0.46205
]

dataTHRUST15 = [
    0.33178,
    0.32144,
    0.31520,
    0.35664,
    0.32614,
    0.31645,
    0.32045,
    0.42106,
    0.31974,
    0.34490
]

data0CUDA20 = [
    3.34160,
    3.34704,
    3.44710,
    3.34477,
    3.34723,
    3.35750,
    3.34365,
    4.24077,
    5.17510,
    5.23891
]

data7CUDA20 = [
    3.41434,
    3.52096,
    3.48506,
    5.25408,
    3.39805,
    3.41488,
    3.43587,
    3.46250,
    3.95798,
    3.43011
]

dataTHRUST20 = [
    3.70070,
    4.79904,
    4.79024,
    4.20598,
    4.78886,
    4.22717,
    3.69958,
    3.70941,
    3.90154,
    4.90480
]

data0CUDA25 = [
    105.30800,
    104.30778,
    96.46375,
    96.83690,
    158.55888,
    157.74223,
    97.41232,
    104.91495,
    105.47639,
    97.33206
]

data7CUDA25 = [
    107.83511,
    104.94224,
    107.37728,
    99.34119,
    99.23981 ,
    106.36861,
    99.21008,
    99.18307,
    99.63917,
    158.82214
]

dataTHRUST25 = [
    119.85181,
    122.52218,
    130.61652,
    120.10902,
    119.82432,
    119.55901,
    131.68115,
    130.38423,
    119.57088,
    119.38864
]

dataTHRUST = [
    9.89670,
    9.81965,
    9.95350,
    9.92774,
    10.12246,
    10.14544,
    9.90762,
    9.91434,
    9.83744,
    9.94230
]

print("data0CUDA15: " + 
    str(np.mean(data0CUDA15)) + 
    " (" + str(np.std(data0CUDA15)) + 
    ") Min: " + str(np.min(data0CUDA15)))

print("data7CUDA15: " + 
    str(np.mean(data7CUDA15)) + 
    " (" + str(np.std(data7CUDA15)) + 
    ") Min: " + str(np.min(data7CUDA15)))

print("dataTHRUST15: " + 
    str(np.mean(dataTHRUST15)) + 
    " (" + str(np.std(dataTHRUST15)) + 
    ") Min: " + str(np.min(dataTHRUST15)))

print("data0CUDA20: " + 
    str(np.mean(data0CUDA20)) + 
    " (" + str(np.std(data0CUDA20)) + 
    ") Min: " + str(np.min(data0CUDA20)))

print("data7CUDA20: " + 
    str(np.mean(data7CUDA20)) + 
    " (" + str(np.std(data7CUDA20)) + 
    ") Min: " + str(np.min(data7CUDA20)))

print("dataTHRUST20: " + 
    str(np.mean(dataTHRUST20)) + 
    " (" + str(np.std(dataTHRUST20)) + 
    ") Min: " + str(np.min(dataTHRUST20)))

print("data0CUDA25: " + 
    str(np.mean(data0CUDA25)) + 
    " (" + str(np.std(data0CUDA25)) + 
    ") Min: " + str(np.min(data0CUDA25)))

print("data7CUDA25: " + 
    str(np.mean(data7CUDA25)) + 
    " (" + str(np.std(data7CUDA25)) + 
    ") Min: " + str(np.min(data7CUDA25)))

print("dataTHRUST25: " + 
    str(np.mean(dataTHRUST25)) + 
    " (" + str(np.std(dataTHRUST25)) + 
    ") Min: " + str(np.min(dataTHRUST25)))
