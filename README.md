# Modern computing technologies

This repository is created to maintain materials for students of the subject `Modern computing technologies`  at Faculty of Physics, Warsaw University of Technology.

Website of the subject: [http://www.if.pw.edu.pl/~gabrielw/NTO.html](http://www.if.pw.edu.pl/~gabrielw/NTO.html).

## Example of combining MPI and BLAS

Go through an example using blas with MPI.

[https://andyspiros.wordpress.com/2011/07/08/an-example-of-blacs-with-c/](https://andyspiros.wordpress.com/2011/07/08/an-example-of-blacs-with-c/)