#ifdef _WIN32
#define WIN32_LEAN_AND_MEAN
#include <windows.h>
#else
#include <unistd.h>
#endif
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <math.h>
#include <iostream>
#include <fstream>

#include <omp.h>
#include <mpi.h>

// user defined includes
#include "covariance.h"
#include <unistd.h>
#include <time.h>

#define ACCURACY 0e-10

double** matrix;
double** covarianceMatrix;

double countCor(double* v1, double* v2, int N){
  double mean1 = 0;
  double mean2 = 0;
  double Cij = 0;
  for(int i=0;i<N;i++){
    mean1+=v1[i];
    mean2+=v2[i];
    Cij+=v1[i]*v2[i];
  }
  return Cij/N - mean1*mean2/(N*N);
}

void printMatrix(int columns) {
  for (int x=0;x<columns;x++) {
    for (int y=0;y<columns;y++) {
      printf("%f ", covarianceMatrix[x][y]);
    }
    printf("\n");
  }
}

int main(int argc, char **argv) {
        
    MPI_Init(&argc, &argv);
    const int N = atoi(argv[3]);
    const int columns = atoi(argv[4]);
    int ip, np;
    MPI_Comm_rank (MPI_COMM_WORLD, &ip);
    MPI_Comm_size (MPI_COMM_WORLD, &np);

    int part = (int)(columns/np);
    int startRange = ip*part;
    int stopRange = (ip+1)*part-1;
    if (ip == np-1) {
      stopRange += columns % np;
      part += columns % np;
    }

    int nthreads = atoi(argv[2]);
    printf("ip=%d \t part size: %d/%d\n",ip,part,columns);
    printf("startRange= %d \t stopRange= %d\n", startRange, stopRange);
    printf("Threads: %d\n", nthreads);
    printf("Processes: %d\n", np);

    // Memory allocation - data
    matrix = (double**)malloc(columns*sizeof(double*));
    for(int i=0;i<columns;i++) 
      matrix[i]=(double*)malloc(N*sizeof(double));
    // Memory allocation - result
    covarianceMatrix = (double**)malloc(columns*sizeof(double*));
    for(int i=0;i<columns;i++) 
      covarianceMatrix[i]=(double*)malloc(columns*sizeof(double));
    // Filling matrix with random numbers
    for (int x=0; x<columns; x++) {
      for (int y=0; y<N; y++)
        matrix[x][y] = (1.0 - 2.0*rand()/((double) RAND_MAX));
    }
    
    MPI_Barrier( MPI_COMM_WORLD );
    if (ip == 0) printf("Memory allocation ended\n");
    printf("ip=%d , [2][2] = %f, [3][3] = %f\n", ip, matrix[2][2], matrix[3][3]);

    sleep(1);
    MPI_Barrier( MPI_COMM_WORLD );
    srand( time(NULL) );

    // force OpenMP to use threads                                                                                                            
    omp_set_dynamic(0);       // Explicitly disable dynamic teams                                                                             
    omp_set_num_threads(nthreads); // Use 4 threads for all consecutive parallel regions                                                             

    MPI_Barrier( MPI_COMM_WORLD );

    double t1, t2, timeDiff;

    t1 = MPI_Wtime();
    // compute covariance - test case and refrence                                                                          
    double** covarianceMatrixPart = covariance_mpi<double>(
      matrix, startRange, stopRange, nthreads, columns, N
    );
    
    for (int i = 0; i <= (stopRange-startRange); i++){
      covarianceMatrix[startRange+i] = covarianceMatrixPart[i];
    }
    
    MPI_Barrier( MPI_COMM_WORLD );

    t2 = MPI_Wtime();
    timeDiff = t2-t1;

    // collect matrix in root process (we arbitraly choose ip=0)                                                                     
    MPI_Gather(matrix, columns, MPI_DOUBLE, matrix, columns, MPI_DOUBLE, 0, MPI_COMM_WORLD);

    //printMatrix(columns);
    //printf("Cor 2-3: %f", countCor(matrix[2], matrix[3],N));

    if (ip == 0) /* only single process */
    {
      //double cov12_serial = covariance_serial<double>(matrix, N);
      printf("\n");
      printf("time diff:    %lf\n", timeDiff);
      printf("\n");
      // Save result to file                                                 
      std::ofstream outfile;
      outfile.open("data.csv", std::ios_base::app);
      outfile << N << ", " << columns << ", " << np << ", " << nthreads << ", " << timeDiff << std::endl;
    }

    MPI_Barrier( MPI_COMM_WORLD );

    /*
    for (int l=0;l<N;l++) 
      free(matrix[l]);
    free(matrix);

    for (int l=0;l<columns;l++) 
      free(covarianceMatrix[l]);
    free(covarianceMatrix);
    
    for (int i = 0; i <= (stopRange-startRange); i++){
      free(covarianceMatrixPart[i]);
    }
    free(covarianceMatrixPart);
    */

    MPI_Finalize();
    
    return EXIT_SUCCESS;
}

