==4627== NVPROF is profiling process 4627, command: ./expected_val.exe
==4627== Profiling application: ./expected_val.exe
==4627== Profiling result:
"Time(%)","Time","Calls","Avg","Min","Max","Name"
%,ms,,ms,ms,ms,
24.620458,29.930024,100,0.299300,0.295898,0.319000,"void kernel_RC_mult<int=256, int=64, int=64, int=1>(double const *, double2 const *, double2*)"
20.024222,24.342579,100,0.243425,0.240410,0.256539,"void kernel_DZdreduce_threadfence<double, unsigned int=128, bool=1>(double const *, double2*, double*, unsigned int)"
19.241633,23.391220,100,0.233912,0.231515,0.239834,"void dot_kernel<double2, double2, double2, int=64, int=1, int=1>(cublasDotParams<double2, double2>)"
17.702458,21.520112,100,0.215201,0.208380,0.226970,"void thrust::system::cuda::detail::bulk_::detail::launch_by_value<unsigned int=128, thrust::system::cuda::detail::bulk_::detail::cuda_task<thrust::system::cuda::detail::bulk_::parallel_group<thrust::system::cuda::detail::bulk_::concurrent_group<thrust::system::cuda::detail::bulk_::agent<unsigned long=7>, unsigned long=128>, unsigned long=0>, thrust::system::cuda::detail::bulk_::detail::closure<thrust::system::cuda::detail::reduce_detail::reduce_partitions, thrust::tuple<thrust::system::cuda::detail::bulk_::detail::cursor<unsigned int=1>, thrust::transform_iterator<thrust::detail::zipped_binary_op<int, evaluate_functional>, thrust::zip_iterator<thrust::tuple<thrust::device_ptr<double>, thrust::device_ptr<double2>, thrust::null_type, thrust::null_type, thrust::null_type, thrust::null_type, thrust::null_type, thrust::null_type, thrust::null_type, thrust::null_type>>, int, thrust::use_default>, thrust::system::cuda::detail::aligned_decomposition<long>, thrust::detail::normal_iterator<thrust::pointer<int, thrust::system::cuda::detail::tag, thrust::use_default, thrust::use_default>>, int, thrust::plus<double>, thrust::null_type, thrust::null_type, thrust::null_type, thrust::null_type>>>>(unsigned long=7)"
14.626186,17.780421,100,0.177804,0.176220,0.184731,"void kernel_DZdreduce<unsigned int=512>(double*, double2*, double*, int)"
1.656746,2.014034,3,0.671344,0.000960,1.346177,"[CUDA memcpy HtoD]"
0.689812,0.838574,100,0.008385,0.008191,0.009312,"void thrust::system::cuda::detail::bulk_::detail::launch_by_value<unsigned int=128, thrust::system::cuda::detail::bulk_::detail::cuda_task<thrust::system::cuda::detail::bulk_::parallel_group<thrust::system::cuda::detail::bulk_::concurrent_group<thrust::system::cuda::detail::bulk_::agent<unsigned long=7>, unsigned long=128>, unsigned long=0>, thrust::system::cuda::detail::bulk_::detail::closure<thrust::system::cuda::detail::reduce_detail::reduce_partitions, thrust::tuple<thrust::system::cuda::detail::bulk_::detail::cursor<unsigned int=1>, thrust::detail::normal_iterator<thrust::pointer<int, thrust::system::cuda::detail::tag, thrust::use_default, thrust::use_default>>, thrust::detail::normal_iterator<thrust::pointer<int, thrust::system::cuda::detail::tag, thrust::use_default, thrust::use_default>>, thrust::detail::normal_iterator<thrust::pointer<int, thrust::system::cuda::detail::tag, thrust::use_default, thrust::use_default>>, thrust::plus<double>, thrust::null_type, thrust::null_type, thrust::null_type, thrust::null_type, thrust::null_type>>>>(unsigned long=7)"
0.462993,0.562841,400,0.001407,0.001343,0.001696,"[CUDA memcpy DtoH]"
0.366355,0.445362,102,0.004366,0.003167,0.120125,"[CUDA memset]"
0.329065,0.400030,100,0.004000,0.003904,0.004288,"void reduce_1Block_kernel<double2, double2, double2, int=64, int=6>(double2*, int, double2*)"
0.280072,0.340472,100,0.003404,0.003264,0.003552,"void __reduce_kernel__<unsigned int=512, double>(double*, double*, int)"

==4627== API calls:
No API activities were profiled.
